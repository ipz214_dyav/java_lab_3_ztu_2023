package com.education.ztu.game;

import java.util.ArrayList;
import java.util.List;

public class Game {
    public static void main(String[] args) {
        Schoolar schoolar1 = new Schoolar("Ivan", 13);
        Schoolar schoolar2 = new Schoolar("Mariya", 15);
        Schoolar schoolar3 = new Schoolar("Mariya2", 51);

        Schoolar schoolar4 = new Schoolar("Sergey", 12);
        Schoolar schoolar5 = new Schoolar("Olga", 14);
        Schoolar schoolar6 = new Schoolar("Ivan", 39);

        Team<Schoolar> scollarTeam = new Team<>("Dragon");
        Team<Schoolar> scollarTeam2 = new Team<>("Rozumnyky");

        scollarTeam.addNewParticipant(schoolar1);
        scollarTeam.addNewParticipant(schoolar2);
        scollarTeam.addNewParticipant(schoolar3);

        scollarTeam2.addNewParticipant(schoolar4);
        scollarTeam2.addNewParticipant(schoolar5);
        scollarTeam2.addNewParticipant(schoolar6);

        scollarTeam.playWith(scollarTeam2);
    }
}
