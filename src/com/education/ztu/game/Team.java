package com.education.ztu.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Team<P> implements Cloneable{
    private String name;
    private List<P> participants = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public void addNewParticipant(P participant) {
        participants.add(participant);
        if(participant instanceof Participant) {
            System.out.println("To the team " + name + " was added participant " + ((Participant) participant).getName());
        } else {
            System.out.println("To the team " + name + " was added participant " + participant);
        }
    }
    public void playWith(Team<P> team) {
        String winnerName;
        Random random = new Random();
        int i = random.nextInt(2);
        if(i == 0) {
            winnerName = this.name;
        } else {
            winnerName = team.name;
        }
        System.out.println("The team " + winnerName + " is winner!");
    }

    public String getName() {
        return name;
    }

    public List<P> getParticipants() {
        return participants;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParticipants(List<P> participants) {
        this.participants = participants;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Team<P> clone(){
        try {
            Team<P> clonedTeam = (Team<P>) super.clone();

            List<P> clonedParticipants = new ArrayList<>();
            for (P participant : participants) {
                if (participant instanceof Cloneable) {
                    clonedParticipants.add((P) ((Participant) participant).clone());
                } else {
                    clonedParticipants.add(participant);
                }
            }

            clonedTeam.setParticipants(clonedParticipants);

            return clonedTeam;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
